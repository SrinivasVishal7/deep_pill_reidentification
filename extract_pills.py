import os
import time
from multiprocessing import Queue, Process

import cv2
import numpy as np

MAX_QUEUE_SIZE = 50000

global frames_queue, pills_queue


def ExtractThreshold(frame, min_thresh=7):
    blur = cv2.medianBlur(frame, 5, 0)
    gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray, min_thresh, 255, cv2.THRESH_BINARY)
    morphological_kernel = np.ones((3, 3), np.uint8)
    eroded_frame = cv2.erode(thresh, morphological_kernel, iterations=2)
    dilated_frame = cv2.dilate(eroded_frame, morphological_kernel, iterations=2)
    return dilated_frame


def get_pill(frame, cnt, crop=600, resize=224):
    hull = cv2.convexHull(cnt)
    M = cv2.moments(hull)
    c_x = int(M["m10"] / M["m00"])
    c_y = int(M["m01"] / M["m00"])
    mask = np.zeros(frame.shape[:2], dtype=frame.dtype)
    mask = cv2.drawContours(mask, [hull], -1, 255, -1)
    out = np.zeros_like(frame)
    out[mask == 255] = frame[mask == 255]
    out = cv2.copyMakeBorder(out, crop, crop, crop, crop, cv2.BORDER_CONSTANT, value=[0, 0, 0])
    c_x = c_x + crop
    c_y = c_y + crop
    pill = out[c_y - int(crop / 2):c_y + int(crop / 2), c_x - int(crop / 2):c_x + int(crop / 2)]
    pill = cv2.resize(pill, (resize, resize), cv2.INTER_AREA)
    return pill


def extract_contours(image, contour, border=50):

    top_left_x, top_left_y, width, height = cv2.boundingRect(contour)
    pill_flag = 0

    if top_left_y - border >= 0 and top_left_y + height + border <= 720 and top_left_x - border >= 0 and top_left_x + width + border <= 1280:

        resizedImage = get_pill(image, contour)
        pill_flag = 1
    else:
        resizedImage = None
        pill_flag = 0
        pass

    return pill_flag, resizedImage


def PillGrabber():
    cam = cv2.VideoCapture('/home/ml/Videos/crocin_slope_1.avi')
    cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
    cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
    cam.set(cv2.CAP_PROP_EXPOSURE, 1)
    while True:
        ret, frame = cam.read()

        if not ret:
            print("No frames from Camera !!")
            cam.release()
            exit(1)

        if frames_queue.qsize() >= MAX_QUEUE_SIZE:
            cam.release()
            print("Queue Size Full. Exiting")
            os.system("pkill -9 python")
            exit(1)

        else:
            frames_queue.put(frame)


def PillProcessor():
    global bg_image, frame_count, start
    i = 0
    frame_count = 0
    bg_taken = False

    while True:

        if frames_queue.qsize() == 0 and bg_taken:
            exit(1)

        while not bg_taken:
            bg_image = frames_queue.get()
            i += 1
            if i == 30:
                print("I am here")
                bg_taken = True

        bgr_frame = frames_queue.get()

        frame_count += 1

        diff_frame = cv2.absdiff(bgr_frame, bg_image)
        thresholded_frame = ExtractThreshold(diff_frame)

        cv2.imshow("Background", diff_frame)
        cv2.waitKey(1)

        contours = cv2.findContours(thresholded_frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[1]
        valid_area_list = []
        valid_contour_list = []

        if len(contours) > 0:
            for cnt in contours:
                area = cv2.contourArea(cnt)
                if area > 7000:
                    valid_area_list.append(area)
                    valid_contour_list.append(cnt)

            for valid_area, valid_cnt in zip(valid_area_list, valid_contour_list):
                pill = get_pill(diff_frame, valid_cnt)
                cv2.imshow("Pill", pill)
                cv2.imwrite("pill" + str(frame_count) + ".bmp", pill)


if __name__ == '__main__':
    frames_queue = Queue(MAX_QUEUE_SIZE)
    pills_queue = Queue(MAX_QUEUE_SIZE)

    Grabber = Process(target=PillGrabber)
    Processor = Process(target=PillProcessor)

    Grabber.start()
    Processor.start()

    Grabber.join()
    Processor.join()

    os.system("pkill -9 python")
