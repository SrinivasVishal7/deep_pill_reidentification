import cv2
import numpy as np
from keras import Model
from keras.applications import VGG19
from keras.applications.imagenet_utils import preprocess_input
from sklearn.externals import joblib

lsAnomaly = joblib.load('/home/ml/PycharmProjects/pillchecker-dev/Pill_FreeFall_Backup/demo_24072018'
                        '/Advance_slope_319_1.pkl')

base_model = VGG19(weights='imagenet')
model = Model(input=base_model.input,
              output=base_model.get_layer('block5_pool').output)


def pre_process_feature(img):
    img = np.expand_dims(img, axis=0)
    img = img.astype('float64')
    img = preprocess_input(img)
    features = model.predict(img).flatten()
    return features


image_sample = './Positive_Pills/pill574.bmp'
image_to_pred = './Negative_Pills/pill45323.bmp'

img_sample = cv2.imread(image_sample)
img_real = cv2.imread(image_to_pred)

features_sample = pre_process_feature(img_sample)
features_real = pre_process_feature(img_real)

pred_true = lsAnomaly.predict_proba(features_sample.reshape(1, -1))
print("Advance : {}".format(pred_true))
pred_negative = lsAnomaly.predict_proba(features_real.reshape(1, -1))
print("Metacin : {}".format(pred_negative))
